// easiest way to run this and test is with `npx ts-node .`

// Create a function that accepts an array, and returns the array in reverse without using array.reverse (JavaScript) or
// array_reverse (PHP). You may use either JavaScript or PHP. Place your answer file in the src/question-1 directory.

/**
 * Returns the reversed array without mutating the original array
 */
function reverseArray<T>(array: T[]) {
  const output: T[] = []

  for (let i = array.length - 1; i >= 0; i--) {
    output.push(array[i])
  }

  return output
}

console.log(reverseArray(['Apple', 'Banana', 'Orange', 'Coconut']))
console.log(reverseArray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
console.log(reverseArray([1, 2, 3, '4', '5']))

// ALTERNATIVE OPTION
// Create a function that determines whether a string is a palindrome. A palindrome is a word or phrase that is spelled
// the same in forwards and reverse. You may use either JavaScript or PHP. Place your answer file in the src/question-1
// directory.

/**
 * returns true if the string is a palindrome
 * disregards spaces and case
 */
function isPalindrome(input: string) {
  input = input.replace(/ /g, '').toLowerCase()

  const reversed = input.split('').reverse().join('')

  return input === reversed
}

console.log(isPalindrome('Level'))
console.log(isPalindrome('Levels'))
console.log(isPalindrome('Yo banana boy'))
