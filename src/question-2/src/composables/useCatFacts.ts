import { ref, computed, onMounted } from 'vue'

import { CatFact } from '../types'

export function useCatFacts() {
  const rawCatFacts = ref<CatFact[]>([])

  const catFacts = computed(() => {
    return rawCatFacts.value.sort((a, b) => a.fact.localeCompare(b.fact))
  })

  onMounted(async () => {
    const res = await fetch('https://catfact.ninja/facts?limit=9')
    const { data }: { data: CatFact[] } = await res.json()
    rawCatFacts.value = data
  })

  return {
    catFacts,
  }
}
