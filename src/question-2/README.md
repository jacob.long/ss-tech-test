# Question 2

To run a local version of the answer for this question, run the following:

```
npm i
```

```
npm run dev
```

---

I used Vite, Vue 3, TypeScript, and vanilla CSS to complete the question
